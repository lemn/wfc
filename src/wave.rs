extern crate png;
extern crate rand;

use crate::image::*;
use std::io::Write;
use std::fmt;
use crate::wave::rand::Rng;
use rand::seq::SliceRandom;

#[derive(Debug)]
pub struct CollapseError;

impl fmt::Display for CollapseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "invalid state reached while collapsing wave function")
    }
}

#[derive(Debug, Clone)]
struct WavePart {
    state: Vec<bool>
}

// A WavePart is a single rectangular part of a larger WaveFunction.
impl WavePart {
    pub fn new(num_states: usize) -> WavePart {
        let state = vec![true;num_states];

        WavePart {
            state
        }
    }

    fn set_state(&mut self, state: Vec<bool> ) {
        self.state = state;
    }

    // returns the index positions of all valid tiles
    fn valid_tiles(&self) -> Vec<usize>{
        self.state
            .iter()
            .enumerate()
            .filter(|&(_, v)| *v )
            .map(|(i, _)| i)
            .collect()
    }

    // Returns the averaged pixel values for the part as an Image.
    // Panics if the part state vector is longer than the provided tile_set
    fn render(&self, tile_set: &[Image]) -> Option<Image> {
        let mut tile_states = Vec::new();

        for (i, b) in self.state.iter().enumerate() {
            if *b { tile_states.push(&tile_set[i])}
        }
        average_images(tile_states)
    }


}

#[derive(Debug)]
pub struct WaveFunction {
    height: usize,
    width: usize,
    tile_width: usize,
    tile_height: usize,
    tile_set: Vec<Image>,
    parts: Vec<WavePart>
}

// A WaveFunction represents an tiled image, where each part is in a superposition of states,
// where each member of tile_set is a possible state.
//
// height and width are in number of parts/tiles in each direction, while tile_height and tile_width 
// are in measured in number of pixels
impl WaveFunction {
    // Returns a new WaveFunction with all parts in a superposition of all provided tile_set
    // images.
    pub fn new(height: usize, width: usize, tile_height: usize, tile_width: usize, tile_set: Vec<Image>) -> WaveFunction {
        let mut states = Vec::new();

        for _ in 0..(height*width) {
            let state = vec![true;tile_set.len()];
            states.push(state);
        }

        let parts = vec![WavePart::new(tile_set.len()); height*width];

        WaveFunction {
            height,
            width,
            tile_height,
            tile_width,
            tile_set,
            parts
        }
    }

    // Returns the waveform rendered as an image, averaging the pixel values 
    // of all current tile states.
    pub fn render(&self) -> Option<Image> {
        let data: Vec<u8> = vec![0;(self.height*self.tile_height)*(self.width*self.tile_width)*3];
        let mut img = Image::new(data, 
                                 (self.width*self.tile_width) as u32,
                                 (self.height*self.tile_height) as u32,
                                 self.tile_set[0].info().color_type,
                                 self.tile_set[0].info().bit_depth,
                                 3);
        let rects = rect_cover(self.tile_height, self.tile_width, self.parts.len(), self.width*self.tile_width);
        for (i, r) in rects.iter().enumerate() {
            let tile_image = self.parts[i].render(&self.tile_set);
            match tile_image {
                Some(tile_image) => img.update_part(r, &tile_image).unwrap(),
                None => return None,
            }

        }

        Some(img)
    }
    
    // Checks if tile t fits in part n based on adjacent parts.
    fn check_tile_fit(&self, n: usize, t: usize) -> bool {
        for side in SIDES {
            let adj_side_pix = self.part_adj_side(n, &side);
            match adj_side_pix {
                None => (),
                Some(pix) => {
                    let mut tile_side_pix = Vec::new();
                    for tile_pix in self.tile_set[t].side(&side) {
                        tile_side_pix.push(vec![tile_pix]);
                    }
                    let pixel_pairs = tile_side_pix.iter().zip(pix.iter());
                    for pair in pixel_pairs {
                        if !super_contains(pair.0, pair.1) {
                            return false
                        }
                    }
                },
            }
        }
        true
    }

    // Returns a superposition of pixel values for each point along the specified edge of part n
    // the superposition of values is represented by a vector of possible Pixels.
    fn part_side(&self, n: usize, side: &Side) -> Vec<Vec<Pixel>> {
        let mut out = Vec::new(); 
        let rect = Rect::new(self.tile_height, self.tile_width, 0);
        let edge_points = rect.side_points(side);

        for p in edge_points.iter() {
            let mut super_pixel = Vec::new();
            for (j, s) in self.parts[n].state.iter().enumerate() {
                if *s {
                    let pixel = self.tile_set[j].pixel(*p);
                    super_pixel.push(pixel);
                }
            }
            super_pixel.sort();
            super_pixel.dedup();
            out.push(super_pixel);
        }

        out
    }

    // Returns the superposition of pixel values for the exterior adjacent
    // edge to part n, on the specified side. If the specified part side is exterior
    // then return None.
    fn part_adj_side(&self, n: usize, side: &Side) -> Option<Vec<Vec<Pixel>>> {
        match side {
            Side::Top => { 
                if n < self.width {
                    None
                } else {
                    Some(self.part_side(n-self.width, &Side::Bottom))
                }
            },
            Side::Bottom => {
                if n >= (self.width * (self.height-1)) {
                    None
                } else {
                    Some(self.part_side(n+self.width, &Side::Top))
                }
            },
            Side::Left => { 
                if n % self.width == 0 {
                    None
                } else {
                    Some(self.part_side(n-1, &Side::Right))
                }
            },
            Side::Right => { 
                if n % self.width == (self.width - 1) {
                    None
                } else {
                    Some(self.part_side(n+1, &Side::Left))
                }
            }, 
        }
    }

    // Part entropy is calculated by finding the number of tiles within the part's
    // current state that have a matching adjacency solution on each side--that is,
    // how many currently available tiles are not excluded as valid possibilities
    // under the constraint that adjacent tile sides must have matching colors.
    fn part_entropy(&self, n: usize) -> usize {
        let mut ent = 0;
        if self.parts[n].state.iter().filter(|&n| *n ).count() == 1 {
            // if only one state is valid, then entropy is 1, don't recalculate
            return 1
        }
        for t in 0..self.tile_set.len() {
            if self.check_tile_fit(n, t) { ent += 1 }
        } 
        ent
    }

    // Return a vector of the entropies for each part of the wavefunction.
    pub fn entropy(&self) -> Vec<usize> {
        let mut entropies = Vec::new();
        for part in 0..self.width*self.height {
            entropies.push(self.part_entropy(part))
        }
        entropies
    }

    // Collapses part of the wavefunction, recalculating and updating the valid states,
    // a true return indicates a state change occurred.
    // Collapses the provided part to its lowest entropy state as determined by
    // adjacent tile restrictions. Will only collapse to a single resulting tile
    // if that is the only tile that meets the pixel-matching restrictions imposed
    // by its adjacent parts.
    pub fn part_collapse(&mut self, n: usize) -> bool {
        let mut state = Vec::new();

        // no action needed if already collapsed to a single state
        if self.parts[n].state.iter().filter(|&n| *n ).count() == 1 {
            return false
        }

        // only check fit for valid tiles
        for t in 0..self.tile_set.len() {
            if self.parts[n].state[t] { 
                state.push(self.check_tile_fit(n, t)) 
            } else {
                state.push(false)
            }
        }

        // don't update if no change in state
        if self.parts[n].state == state {
            false
        } else {
            self.parts[n].set_state(state);
            true
        }
    }

    // Collapses all parts of the wavefunction, in index order, to their 
    // lowest entropy state based on the state of adjacent tiles. This will
    // Continues collapsing parts until no further state changes occur.
    pub fn collapse(&mut self) -> Result<(), CollapseError> {
        let mut state_changed = false;
        print!(".");
        std::io::stdout().flush().unwrap();
        for n in 0..self.width*self.height {
            state_changed = self.part_collapse(n);
        }
        if state_changed { self.collapse()?}
        if *self.entropy().iter().min().unwrap() == 0 {
            return Err(CollapseError)
        }
        Ok(())
    }
    
    // Forces the collapse of part n to a randomly chosen valid tile.
    // If the choice of random tile results in an invalid wavefunction state
    // (any tiles having 0 entropy) then a different random tile will be chosen 
    // and the process will repeat until either the ensuing collapse results in a 
    // valid wavefunction state or all tile choices are exhausted.
    // Returns a CollapseError in the latter case, and a unit Result in the former.
    pub fn force_part_collapse(&mut self, n: usize) -> Result<(), CollapseError> {
        let mut valid_tiles = self.parts[n].valid_tiles();
        while !valid_tiles.is_empty() {
            let rand_tile = rand::thread_rng().gen_range(0..valid_tiles.len());
            let mut new_state = vec![false;self.tile_set.len()];
            new_state[valid_tiles[rand_tile]] = true;
            self.parts[n].set_state(new_state);
            let saved = self.save_states();
            match self.collapse() {
                Ok(()) => { 
                    return Ok(()) 
                },
                Err(CollapseError) => { 
                    println!("invalid tile, retrying with new selection");
                    valid_tiles.remove(rand_tile);
                    self.load_states(saved);
                    continue
                    }
                };
            }
            Err(CollapseError)
        }

    // Forces the entire wavefunction to collapse to a single value.
    // Collapse starts with the part having minimum (non-unity) entropy,
    // chosen at random between tying parts. If the attempted collapse 
    // of a given part inevitably leads to an invalid state (any tiles
    // having 0 entropy) then a different part will be chosen until either
    // the ensuing collapse results in a fully collapsed wavefunction or 
    // all part choices are exhausted.
    // Returns a CollapseError in the latter case, and in the former, 
    // a vector of images corresponding to each step of the collapse.
    pub fn force_collapse(&mut self) -> Result<Vec<Image>, CollapseError> {
        let mut rendered = Vec::new(); 
        let entropy = self.entropy();

        // Only parts with entropy > 1 can be chosen for collapse.
        let mut valid_parts: Vec<(usize,usize)> = entropy
            .into_iter()
            .enumerate()
            .filter(|&(_,v)| v != 1)
            .collect();
        // If force_part_collapse fails for a chosen part, it will be
        // removed from valid_parts and a new part will be chosen from it.
        // If valid_parts is empty, then no further collapse is possible.
        while !valid_parts.is_empty() {
            let entropy = self.entropy();
            let min_entropy = entropy // minimum non-unity entropy value
                .iter()
                .filter(|&v| *v != 1)
                .min();
            match min_entropy {
                None => return Ok(rendered), // no non-zero parts remain
                Some(min) => {
                    // Recalculate valid parts, some parts could have collapsed
                    // to entropy = 1 without being forced so we need to catch those.
                    valid_parts = self.entropy()
                        .into_iter()
                        .enumerate()
                        .filter(|&(_,v)| v != 1)
                        .collect();

                    // Choose a random valid part.
                    // TODO: it's possible to re-choose the same part as a previous
                    // faied loop due to the above recalculation of valid_parts, which
                    // can lead to wasted attempts. Should find a different way to update
                    // this var to avoid this.
                    let rand_part = *valid_parts
                        .iter()
                        .filter(|(_,v)| v == min)
                        .map(|(i,_)| i)
                        .collect::<Vec<&usize>>()
                        .choose(&mut rand::thread_rng())
                        .unwrap();
                    
                    // save the wavefunction state before collapse in case rollback is needed
                    let saved = self.save_states();
                    match self.force_part_collapse(*rand_part) {
                        Ok(()) => {
                            // render the current wavefunction as an image and continue
                            let frame = self.render().unwrap();
                            rendered.push(frame);
                            continue
                        }
                        Err(CollapseError) => {
                            // no more valid parts remain, return error
                            if valid_parts.len() == 1 {
                                return Err(CollapseError)
                            }
                            println!("invalid part, retrying with new selection");
                            valid_parts.remove(*rand_part);
                            // rollback to previous state before trying a new part
                            self.load_states(saved);
                            continue
                        }
                    };
                }
            }
        }
        let max_entropy = *self.entropy().iter().max().unwrap();
        if max_entropy == 1 {
            Ok(rendered)
        } else {
            Err(CollapseError)
        }
    }

    // Saves the state of all parts, returning it a a Vector of boolean 
    // vectors (one boolean for each state, one vector of bools for each part).
    pub fn save_states(&self) -> Vec<Vec<bool>> {
        let mut out = Vec::new();
        for p in self.parts.iter() {
            out.push(p.state.clone())
        }
        out
    }

    // Loads a saved state of all parts.
    pub fn load_states(&mut self, states: Vec<Vec<bool>>) {
        for (n, state) in states.into_iter().enumerate() {
            self.parts[n].set_state(state);
        }
    }


}


// Takes two superpositions and returns true if there are any
// elements that exist in both.
fn super_contains<T: std::cmp::PartialEq>(a: &[T], b: &[T]) -> bool {
    for item in a {
        if b.contains(item) {
            return true
        } 
    }
    false
}

