    use std::cmp::Ordering;
    use png::Decoder;
    use std::fs::File;
    use std::path::Path;
    use std::io::BufWriter;

    pub const SIDES: [Side; 4] = [Side:: Top, Side::Bottom, Side::Left, Side::Right];

    #[derive(Eq, PartialEq, Ord, PartialOrd, Debug)]
    pub struct Pixel {
        r: u8,
        g: u8,
        b: u8
    }

    // Side allows for the specification of a specific rectangles side
    // useful for calculating edge or adjacency values
    #[derive(Hash, Eq, PartialEq, Debug)]
    pub enum Side {
        Top,
        Bottom,
        Left,
        Right
    }

    // Rect represents a rectangle with specified dimensions and an offset starting value
    #[derive(Debug)]
    pub struct Rect {
        height: usize,
        width: usize,
        offset: usize
    }

    impl Rect  {

        // create a new Rect
        pub fn new(height: usize, width: usize, offset: usize) -> Rect {
            Rect { height, width, offset }
        }

        // Returns array index values for the rectangle give an initial offset and
        // maximum width. If the offset & rectangle width exceed the max width
        // then the offset will be effectively reduced to disallow the rectangle to
        // exceed the specified horizontal bound
        //
        // Panics if max_width is smaller than the underlying rectangle width
        pub fn indices(&self, max_width: usize) -> Vec<usize> {
            let mut out: Vec<usize> = Vec::new();
            let mut count = 0;

            // if not enough pixels remain in the row
            // move offset back to prevent wrapping
            let mut offset = self.offset;
            if self.width >= max_width - (offset % max_width)  {
                offset = offset - ((offset % max_width) + self.width - max_width);
            }

            while count < self.height*self.width {
                // (offset + (tile column #) + (tile row # * row width))
                out.push((offset)+((count%self.width)+(count/self.width)*(max_width)));
                count += 1;
            }
            out
        }

        // Returns array index values for the requested side of the rectangle
        pub fn side_points(&self, side: &Side) -> Vec<usize> {
            let mut out = Vec::new();
            match side {
                Side::Top => for t in 0..self.width { out.push(t+self.offset) },
                Side::Bottom => for p in (self.width-1)*self.height..self.width*self.height { out.push(p+self.offset) },
                Side::Left => for p in 0..self.height { out.push(p*self.width+self.offset) },
                Side::Right => for p in 0..self.height { out.push(p*self.height + self.width +self.offset - 1) },
            }
            out
        }
    }

    // Image collects the png::OutputInfo with its associated byte data
    #[derive(Eq, PartialEq, Debug)]
    pub struct Image {
        data: Vec<u8>,
        info: png::OutputInfo
    }

    impl Ord for Image {
        fn cmp(&self, other: &Self) -> Ordering {
            (self.data).cmp(&other.data)
        }
    }

    impl PartialOrd for Image {
        fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Image {

        // create a new Image
        pub fn new(data: Vec<u8>, 
                   width: u32, 
                   height: u32, 
                   color_type: 
                   png::ColorType, 
                   bit_depth: 
                   png::BitDepth, 
                   pixel_bytes: usize) -> Image {

            Image { 
                data, 
                info: png::OutputInfo {
                    width,
                    height,
                    color_type,
                    bit_depth,
                    line_size: width as usize * pixel_bytes,
                }
            } 

        }

         // returns the nth Pixel
        pub fn pixel(&self, num: usize) -> Pixel {
            let r_coord = num*3;
            let g_coord = num*3+1;
            let b_coord = num*3+2;
            Pixel { r: self.data[r_coord] , g: self.data[g_coord], b: self.data[b_coord] }
        }

        // returns the pixel with provided x,y coordinates where 0,0 
        // corresponds to pixel(0)--the top-left pixel of the image
        pub fn pixel_xy(&self, x: usize, y: usize) -> Pixel {
            self.pixel(x + (y * self.info.width as usize))
        }

        // returns the cropped image value corresponding to the provided rectangle
        pub fn crop(&self, rect: &Rect) -> Image {
            let mut out: Vec<u8> = Vec::new();
            let points = rect.indices(self.info.width as usize);
            for point in points {
                let pix = self.pixel(point);
                let mut data = vec![pix.r, pix.b, pix.g];
                out.append(&mut data);
            }
            Image::new(out, rect.width as u32, rect.height as u32, self.info.color_type, self.info.bit_depth, 3)
        } 

        // info getter
        pub fn info(&self) -> &png::OutputInfo {
            &self.info
        }

        // returns a vector of pixel values of the provided side
        // reads top->down, left->right
        pub fn side(&self, side: &Side) -> Vec<Pixel> {
            let mut out = Vec::new();
            let rect = Rect{ height: self.info.height as usize, width: self.info.width as usize, offset: 0 };
            for p in rect.side_points(side) { out.push(self.pixel(p)) }
            out
        }

        // replaces the data in the part corresponding to the provided rectangle with the provided tile
        pub fn update_part(&mut self, rect: &Rect, tile: &Image) -> Result<(), String> {
            let points = rect.indices(self.info.width as usize);
            for (count, point) in points.iter().enumerate() {
                let target_pix = tile.pixel(count);
                self.data[(point*3)] = target_pix.r;
                self.data[(point*3)+1] = target_pix.b;
                self.data[(point*3)+2] = target_pix.g;
            }
            Ok(())
        }

    }

    // returns a covering of count # of rectangles with specified height and width 
    // using the provided horiz_bound to calculate the resulting offsets
    pub fn rect_cover(height: usize, width: usize, count: usize, horiz_bound: usize) -> Vec<Rect> {
        let mut rects = Vec::new();

        for c in 0..count {
            let h_offset = width * (c % (horiz_bound/width));
            let v_offset = (height*horiz_bound) * (c/(horiz_bound/width));
            rects.push(Rect {height, width, offset: h_offset + v_offset});

        }
        rects
    }

    // splits an image in into tiles of specified height and width
    pub fn split_image(image: Image, tile_height: usize, tile_width: usize) -> Vec<Image> {
        let mut tile_set: Vec<Image> = Vec::new();

        let count = (image.info.height as usize/tile_height)*(image.info.width as usize/tile_width);
        let rects = rect_cover(tile_height, tile_width, count, image.info.width as usize);
        for rect in rects {
            let tile = image.crop(&rect);
            tile_set.push(tile);
        }

        tile_set.sort();
        tile_set.dedup();

        tile_set
    }

    // loads a png image file from the specified path, returning an Image
    pub fn load_png(file: &str) -> Image {
        let decoder = Decoder::new(File::open(file).unwrap());
        let mut reader = decoder.read_info().unwrap();
        let mut buf = vec![0; reader.output_buffer_size()];

        let info = reader.next_frame(&mut buf).unwrap();
        Image {
            data: buf[..info.buffer_size()].to_vec(),
            info
        }
    }

    // saves a png Image to the provided path
    pub fn save_png(path: &str, img: &[Image]) {
        let path = Path::new(path);
        let file = File::create(path).unwrap();
        let w = &mut BufWriter::new(file);
        let mut encoder = png::Encoder::new(w, img[0].info.width, img[0].info.height);
        encoder.set_color(img[0].info.color_type);
        encoder.set_depth(img[0].info.bit_depth);
        if img.len() > 1 {
            encoder.set_animated(img.len() as u32, 0).unwrap();
            encoder.set_frame_delay(1, 12).unwrap();
        }
        let mut writer = encoder.write_header().unwrap();

        for i in img {
            let data = i.data.as_slice();
            writer.write_image_data(data).unwrap();
        }
    }
    // take a slice of Images and return the result of averaging their pixel values
    // each image must be the same size, or else the output will either not reflect
    // the full size of each image, or will panic if an out-of-bounds pixel access is attempted
    pub fn average_images(imgs: Vec<&Image>) -> Option<Image> {
        let mut data = Vec::new();

        if imgs.is_empty() {
            return None
        }

        let mut count = 0;
        while count < (imgs[0].data.len())/3 {
            let (mut r_val, mut g_val, mut b_val): (usize, usize, usize) = (0, 0, 0);
            for img in &imgs[0..] {
                let pixel = img.pixel(count);
                r_val += pixel.r as usize;
                g_val += pixel.g as usize;
                b_val += pixel.b as usize;
            }
            data.push((r_val/imgs.len()) as u8);
            data.push((g_val/imgs.len()) as u8);
            data.push((b_val/imgs.len()) as u8);
            count += 1;
        }
        let info = &imgs[0].info;

        Some(Image { data, info: png::OutputInfo { .. *info } })
    }

    // returns the image flipped horizontally
    pub fn flip_horizontal(img: &Image) -> Image {
        let mut data = Vec::new();
        let info = &img.info;

        let mut count = 0;
        while count < img.data.len()/3 {
            let y = count / img.info.height as usize;
            let x = count % img.info.width as usize;
            let pixel = img.pixel_xy(img.info.width as usize - x - 1, y); 
            data.push(pixel.r);
            data.push(pixel.g);
            data.push(pixel.b);
            count += 1;
        }
        Image {data, info: png::OutputInfo { .. *info }}
    }

    // returns the image flipped vertically
    pub fn flip_vertical(img: &Image) -> Image {
        let mut data = Vec::new();
        let info = &img.info;

        let mut count = 0;
        while count < img.data.len()/3 {
            let y = count / img.info.height as usize;
            let x = count % img.info.width as usize;
            let pixel = img.pixel_xy(x, img.info.height as usize - y - 1); 
            data.push(pixel.r);
            data.push(pixel.g);
            data.push(pixel.b);
            count += 1;
        }
        Image {data, info: png::OutputInfo { .. *info }}
    }

    // returns the image flipped on the rising diagonal
    pub fn flip_echelon(img: &Image) -> Image {
        let mut data = Vec::new();
        let info = &img.info;

        let mut count = 0;
        while count < img.data.len()/3 {
            let y = count / img.info.height as usize;
            let x = count % img.info.width as usize;
            let pixel = img.pixel_xy(img.info.height as usize - y - 1, img.info.width as usize - x - 1); 
            data.push(pixel.r);
            data.push(pixel.g);
            data.push(pixel.b);
            count += 1;
        }
        Image {data, info: png::OutputInfo { .. *info }}
    }

    // returns the image rotated 90 deg to the right
    pub fn rrot_90(img: &Image) -> Image {
        let mut data = Vec::new();
        let info = &img.info;

        let mut count = 0;
        while count < img.data.len()/3 {
            let y = count / img.info.height as usize;
            let x = count % img.info.width as usize;
            let pixel = img.pixel_xy(img.info.width as usize - x - 1, y); 
            data.push(pixel.r);
            data.push(pixel.g);
            data.push(pixel.b);
            count += 1;
        }
        Image {data, info: png::OutputInfo { .. *info }}
    }

    // returns the image rotated 180 deg to the right
    pub fn rrot_180(img: &Image) -> Image {
        let mut data = Vec::new();
        let info = &img.info;

        let mut count = 0;
        while count < img.data.len()/3 {
            let y = count / img.info.height as usize;
            let x = count % img.info.width as usize;
            let pixel = img.pixel_xy(img.info.width as usize - x - 1, img.info.height as usize - y - 1); 
            data.push(pixel.r);
            data.push(pixel.g);
            data.push(pixel.b);
            count += 1;
        }
        Image {data, info: png::OutputInfo { .. *info }}
    }

    // returns the image rotated 270 deg to the right
    pub fn rrot_270(img: &Image) -> Image {
        let mut data = Vec::new();
        let info = &img.info;

        let mut count = 0;
        while count < img.data.len()/3 {
            let y = count / img.info.height as usize;
            let x = count % img.info.width as usize;
            let pixel = img.pixel_xy(img.info.height as usize - y - 1, x); 
            //let pixel = img.pixel_xy(img.info.width as usize - x - 1, img.info.height as usize - y - 1); 
            data.push(pixel.r);
            data.push(pixel.g);
            data.push(pixel.b);
            count += 1;
        }
        Image {data, info: png::OutputInfo { .. *info }}
    }

#[cfg(test)]
mod test {

    use crate::image::Image;
    fn create_test_img() -> Image {
        let black: [u8; 3] = [0, 0, 0];
        let white: [u8; 3] = [255, 255, 255];
        let data: Vec<u8> = vec!(black, black, white, black, black, white, black, black, white, black, white, white, black, black, white, white).into_iter().flatten().collect();
        Image::new(data, 4, 4, png::ColorType::Rgb, png::BitDepth::Eight, 3)
    }
        
    #[test]
    fn test_rect_cover_3x3x12() {
        use crate::image::rect_cover;

        let cover = rect_cover(3, 3, 16, 12);
        
        assert_eq!(cover[0].height, 3);
        assert_eq!(cover[0].width, 3);
        assert_eq!(cover.len(), 16);
        assert_eq!(cover[4].offset, 36);
        assert_eq!(cover[8].offset, 72);
    }

    #[test]
    fn test_rect_sides() {
        use crate::image::rect_cover;
        use crate::image::Side;

        let cover = rect_cover(3, 3, 16, 12);
        let top_side = cover[6].side_points(&Side::Top);
        let bottom_side = cover[6].side_points(&Side::Bottom);
        let left_side = cover[6].side_points(&Side::Left);
        let right_side = cover[6].side_points(&Side::Right);
        
        assert_eq!(top_side, vec!(42, 43, 44));
        assert_eq!(bottom_side, vec!(48, 49, 50));
        assert_eq!(left_side, vec!(42, 45, 48));
        assert_eq!(right_side, vec!(44, 47, 50));
    }

    #[test]
    fn test_rect_indices() {
        use crate::image::Rect;

        assert_eq!(Rect::new(2, 3, 0).indices(3), vec!(0,1,2,3,4,5)); // max_width = width
        assert_eq!(Rect::new(2, 3, 0).indices(4), vec!(0,1,2,4,5,6)); // width < max_width
        assert_eq!(Rect::new(2, 3, 1).indices(4), vec!(1,2,3,5,6,7)); // width < max_width, offset = 1
        assert_eq!(Rect::new(2, 3, 2).indices(4), vec!(1,2,3,5,6,7)); // width < max_width, offset = 2, prevent wrap
        assert_eq!(Rect::new(2, 3, 3).indices(4), vec!(1,2,3,5,6,7)); // width < max_width, offset = 3, prevent wrap
        assert_eq!(Rect::new(2, 3, 5).indices(4), vec!(5,6,7,9,10,11)); // offset > max_width
        assert_eq!(Rect::new(2, 3, 6).indices(4), vec!(5,6,7,9,10,11)); // offset > max_width, prevent wrap
    }

    #[test]
    #[should_panic]
    fn test_rect_indices_panic() {
        use crate::image::Rect;
        assert_eq!(Rect::new(2, 3, 0).indices(2), vec!(0,1,2,4,5,6)); // width > max_width, should panic
    }

    #[test]
    fn test_flip_horizontal() {
        use crate::image::flip_horizontal;
        let img = create_test_img();
        let testimg = flip_horizontal(&img);

        assert_eq!(testimg.data, vec!(0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 0, 0, 0, 0, 0));
    }

    #[test]
    fn test_flip_vertical() {
        use crate::image::flip_vertical;
        let img = create_test_img();
        let testimg = flip_vertical(&img);

        assert_eq!(testimg.data, vec!(0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0));
    }

    #[test]
    fn test_flip_echelon() {
        use crate::image::flip_echelon;
        let img = create_test_img();
        let testimg = flip_echelon(&img);

        assert_eq!(testimg.data, vec!(255, 255, 255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0));
    }

    #[test]
    fn test_rrot90() {
        use crate::image::rrot_90;
        let img = create_test_img();
        let testimg = rrot_90(&img);

        assert_eq!(testimg.data, vec!(0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 0, 0, 0, 0, 0));
    }

    #[test]
    fn test_rrot180() {
        use crate::image::rrot_180;

        let img = create_test_img();
        let testimg = rrot_180(&img);

        assert_eq!(testimg.data, vec!(255, 255, 255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0));
    }

    #[test]
    fn test_rrot270() {
        use crate::image::rrot_270;
        let img = create_test_img();
        let testimg = rrot_270(&img);

        assert_eq!(testimg.data, vec!(0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 255, 255, 255, 0, 0, 0, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 0, 0, 0));
    }
}

