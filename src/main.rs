mod wave;
mod image;

use chrono::prelude::*;
use clap::Parser;

#[derive(Parser, Debug)]
struct Cli {
    #[clap(long = "tile_height", default_value = "3")]
    tile_height: usize,

    #[clap(long = "tile_width", default_value = "3")]
    tile_width: usize,

    #[clap(long = "height")]
    height: usize,
    
    #[clap(long = "width")]
    width: usize,

    // #[clap(long = "save_tiles", takes_value = false)]
    // save_tiles: bool,

    #[clap(long = "vflip", takes_value = false)]
    vflip: bool,

    #[clap(long = "hflip", takes_value = false)]
    hflip: bool,

    #[clap(long = "eflip", takes_value = false)]
    eflip: bool,

    #[clap(long = "rot90", takes_value = false)]
    rot90: bool,

    #[clap(long = "rot180", takes_value = false)]
    rot180: bool,
    
    #[clap(long = "rot270", takes_value = false)]
    rot270: bool,
    
    #[clap(long = "input")]
    input_image: String, 
}

fn main() {

    let cli_args = Cli::parse();

    let tile_height = cli_args.tile_height;
    let tile_width = cli_args.tile_width;
    let height = cli_args.height;
    let width = cli_args.width;

    let img_path = cli_args.input_image;

    // if cli_args.save_tiles {
    //     println!("save tiles = {:?}", cli_args.save_tiles);
    //     // save the generated tiles for inspection
    //     println!("loading input image {}", img_path);
    //     let img = image::load_png(&img_path);
    //     let split_tiles = image::split_image(img, tile_height, tile_width);
    //     let mut tile_set = Vec::new();
    //     for img in split_tiles {
    //         if cli_args.vflip {
    //             tile_set.push(image::flip_vertical(&img));
    //         }
    //         if cli_args.hflip {
    //             tile_set.push(image::flip_horizontal(&img));
    //         }
    //         if cli_args.eflip {
    //             tile_set.push(image::flip_echelon(&img));
    //         }
    //         if cli_args.rot90 {
    //             tile_set.push(image::rrot_90(&img));
    //         }
    //         if cli_args.rot180 {
    //             tile_set.push(image::rrot_180(&img));
    //         }
    //         if cli_args.rot270 {
    //             tile_set.push(image::rrot_270(&img));
    //         }
    //         tile_set.push(img);
    //     }
    //     tile_set.sort();
    //     tile_set.dedup();
    //
    //     let base_path = "img/tiles/tile";
    //     let suffix = ".png";
    //
    //     let mut count = 0;
    //     while count < tile_set.len() {
    //         let mut out = String::from(base_path);
    //         out.push_str(&count.to_string());
    //         out.push_str(suffix);
    //         println!("saving {}...", out);
    //         image::save_png(&out, &tile_set[count..count+1]);
    //         count += 1;
    //     }
    // }

    // wavefunction creation & collapse
    println!("Starting WaveFunction collapse");
    let mut loop_count = 1;
    let mut rendered = Vec::new();
    'outer: loop {
        let img = image::load_png(&img_path);
        let split_tiles = image::split_image(img, tile_height, tile_width);
        let mut tile_set = Vec::new();
        for img in split_tiles {
            if cli_args.vflip {
                tile_set.push(image::flip_vertical(&img));
            }
            if cli_args.hflip {
                tile_set.push(image::flip_horizontal(&img));
            }
            if cli_args.eflip {
                tile_set.push(image::flip_echelon(&img));
            }
            if cli_args.rot90 {
                tile_set.push(image::rrot_90(&img));
            }
            if cli_args.rot180 {
                tile_set.push(image::rrot_180(&img));
            }
            if cli_args.rot270 {
                tile_set.push(image::rrot_270(&img));
            }
            tile_set.push(img);
        }
        tile_set.sort();
        tile_set.dedup();

        println!("\nAttempt #{}", loop_count);
        let mut wavefunc = wave::WaveFunction::new(height, width, tile_height, tile_width, tile_set);
        
        wavefunc.collapse().unwrap();
        rendered.push(wavefunc.render().unwrap());
            let res = wavefunc.force_collapse();
            match res {
                Ok(mut rend) => {
                    rendered.append(&mut rend);
                    let now = Utc::now();
                    let anim_out = &format!("img/anim_wf_{}_attempt_{}.png", now.format("%Y%m%d%H%M%S"), loop_count);
                    let final_out = &format!("img/wf_{}_attempt_{}.png", now.format("%Y%m%d%H%M%S"), loop_count);
                    println!("success!");
                    println!("saving animation to {}...", anim_out);
                    println!("saving final frame to {}...", final_out);
                    image::save_png(anim_out, &rendered);
                    image::save_png(final_out, &rendered[rendered.len()-1..]);
                    break
                },
                Err(_) => {
                    loop_count += 1;
                    println!("invalid configuration, retrying...");
                    continue 'outer
                }
            }
        }
    // }
}

