# Wave Function Collapse

Implementing the Wave Function Collapse algorithm (https://github.com/mxgmn/WaveFunctionCollapse) in Rust as a practice exercise.

Reads in a png file (non-transparent) and splits it into non-overlapping NxN tiles (currently configured via statics in main()) and uses those to generate a new, similar, image on a grid of NxN tiles and saves the results as an animated gif showing each step of the wavefunction collapse.

Example:

For the input file of  
![blocks](img/blocks.png)  
the following output was generated (zoomed for better viewing)  
![wfc](img/wavefunction_l.png)
